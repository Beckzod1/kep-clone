import { CommonModule, } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewChild, inject } from '@angular/core';
import { HttpClient, HttpClientModule, provideHttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { MatPaginatorModule } from '@angular/material/paginator'
import { MatTableDataSource, MatTableModule } from '@angular/material/table'
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatInputModule } from '@angular/material/input'
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelect, MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';
import { FormsModule, NgModel } from '@angular/forms';

@Component({
  selector: 'app-problemstable',
  standalone: true,
  templateUrl: './problemstable.component.html',
  styleUrl: './problemstable.component.css',
  imports: [
    CommonModule,
    HttpClientModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule,
    FormsModule,


  ]
})
export class ProblemstableComponent implements OnInit, AfterViewInit {
  http = inject(HttpClient);
  pageSizeOptions: any;
  lengthValue: any;

  filterOptions: string[] = ['All', 'Yes', 'No'];
  selectedFilterOption: string = 'All';
  filterValue: string = '';
  displayedColumns: string[] = ['id', 'title', 'difficultyTitle', 'solved', 'tagsName'];
  dataSource = new MatTableDataSource<any>();

  constructor(private apiService: ApiService) { }

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  ngOnInit(): void {
    this.loadData(1);
  }
  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;










  }

  loadData(page: number): void {
    this.apiService.getProblems(page).subscribe((res) => {
      this.dataSource.data = res.data;
      this.lengthValue = res.total;
      this.pageSizeOptions = res.count
    });
  }

  onPageChange(event: any): void {
    const page = event.pageIndex + 1;
    this.loadData(page);
  }


  //All data filter search
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value.trim().toLowerCase();
    this.dataSource.filter = filterValue;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  // Custom filter predicate for 'isSolution' column
  customFilterPredicate(data: any, checkfilter: string): boolean {
    if (checkfilter === 'All') {
      return true; // Show all rows
    }

    const isSolution = data.hasSolution ? 'Yes' : 'No';
    return isSolution === checkfilter;
  }

  selectionFilter() {
    // Set the filter predicate before applying the filter
    //this.dataSource.filterPredicate = (data, filter) => this.customFilterPredicate(data, filter);

    // Apply the filter based on the selected option
    this.dataSource.filter = this.selectedFilterOption === 'All' ? '' : this.selectedFilterOption;


    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }




}





