import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProblemstableComponent } from './problemstable.component';

describe('ProblemstableComponent', () => {
  let component: ProblemstableComponent;
  let fixture: ComponentFixture<ProblemstableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ProblemstableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ProblemstableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
