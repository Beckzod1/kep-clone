interface Tag {
    id: number;
    name: string;
}



interface Problem {
    id: number;
    title: string;
    difficulty: number;
    solved: number;
    tags: Tag[];
}

export interface ApiResponse {
    data: Problem[];
}
