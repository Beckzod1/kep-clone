import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse } from './problemstable/types';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProblemsService {
  private apiUrl = 'https://kep.uz/api/problems'

  constructor(private http: HttpClient) { }

  getProblems() {
    return this.http.get(this.apiUrl);
  }





}
