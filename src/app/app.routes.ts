import { Component } from '@angular/core';
import { Routes } from '@angular/router';
import { ProblemstableComponent } from './problemstable/problemstable.component';

export const routes: Routes = [
    { path: "problems_table", component: ProblemstableComponent }
];
